﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherScript : MonoBehaviour
{
    Rigidbody rb;
    public float pushspeed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("Destroy", 3f);
    }
    private void FixedUpdate()
    {
        rb.velocity = transform.forward * pushspeed;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void Destroy()
    {
        Destroy(this.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherSpawner : MonoBehaviour
{
    public GameObject pusherPrefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreatePusher", 2f, 2f);
    }
    void CreatePusher()
    {
        //instantiate pusher prefab at our position & rotation
        Instantiate(pusherPrefab, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

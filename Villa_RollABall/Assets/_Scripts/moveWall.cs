﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveWall : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    bool goForward = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {//change movement direction depending on boolean
        if (goForward) { rb.velocity = transform.forward * speed; }
        
        else{ rb.velocity = -transform.forward * speed; }
    }
    private void OnCollisionEnter(Collision collision)
    {//determine if hitting wall, if yes then switch direction
        if (collision.gameObject.CompareTag("wall"))
        {
            print("HIT WALL");
            if (goForward)
            {
                print("GO BACKWARD");
                
                goForward = false;
            }
            else
            {
                goForward = true;
                print("GO FORWARD");
            }
            
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

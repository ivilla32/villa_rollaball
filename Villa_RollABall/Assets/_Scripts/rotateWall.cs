﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateWall : MonoBehaviour
{
    //variables
    Rigidbody rb;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(Vector3.right * Time.deltaTime * speed);
        rb.angularVelocity = new Vector3(0f, Time.deltaTime * speed, 0f);
    }
}
